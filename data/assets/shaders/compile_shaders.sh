#!/bin/bash

cd $(dirname $0)
glslc textured_mesh.vert -o textured_mesh_vert.spv
glslc textured_mesh.frag -o textured_mesh_frag.spv
glslc basic_mesh.vert -o basic_mesh_vert.spv
glslc basic_mesh.frag -o basic_mesh_frag.spv