#version 450

layout(location = 0) in vec3 input_colour;

layout(location = 0) out vec4 output_colour;

void main() {
    output_colour = vec4(input_colour, 1.0);
}