#version 450

layout(location = 0) in vec3 input_position;
layout(location = 1) in vec3 input_colour;
layout(location = 2) in vec3 input_normal;
layout(location = 3) in vec2 input_texture_coords;

layout(location = 0) out vec3 output_colour;
layout(location = 1) out vec2 output_texture_coords;

layout(push_constant) uniform constants {
    mat4 matrix;
} push_constants;

void main() {
    gl_Position = push_constants.matrix * vec4(input_position, 1.0);

    output_colour = input_colour;
    output_texture_coords = input_texture_coords;
}