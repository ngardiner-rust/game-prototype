#version 450

layout(location = 0) in vec3 input_colour;
layout(location = 1) in vec2 input_texture_coords;

layout(location = 0) out vec4 output_colour;

layout(set = 0, binding = 0) uniform sampler2D input_texture;

void main() {
    output_colour = texture(input_texture, input_texture_coords);
}