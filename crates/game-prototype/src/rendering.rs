use std::{cell::RefCell, error::Error, rc::Rc, collections::HashMap, path::Path };

use slotmap::{SlotMap, SecondaryMap};
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent, DeviceEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

use nalgebra_glm as glm;

use imgui_rs_vulkan_renderer as imgui_vk;

use crate::camera::*;
use crate::assets::*;
use rendering_engine::*;

const MAX_FRAMES_IN_FLIGHT: usize = 2;

pub struct Renderer {
    m_renderables: Vec<Renderable>,

    m_shaders: HashMap<String, Rc<Shader>>,

    m_assets: Assets,

    m_frames: Vec<FrameResources>,
    m_current_frame: usize,
    m_depth_image: Image,
    m_descriptor_manager: Rc<RefCell<DescriptorManager>>,
    m_viewport_state: ViewportState,
    m_logical_device: Rc<Device>,
    m_surface: Surface,
    m_instance: Instance,

    m_resized_this_frame: bool,

    m_camera: Rc<RefCell<Camera>>,
}

impl Renderer {
    pub fn new(window: &Window, camera: Rc<RefCell<Camera>>) -> Result<Self, Box<dyn Error>> {
        log::info!("Started renderer initialisation.");

        let mut instance_builder = Instance::builder()
            .application_name(String::from("Game Prototype"))
            .application_version(ash::vk::make_api_version(0, 1, 0, 0))
            .window_extension_names(ash_window::enumerate_required_extensions(window).unwrap());

        // All layers should be disabled for release builds.
        if cfg!(debug_assertions) {
            instance_builder = instance_builder.required_layer_names(vec![
                #[cfg(debug_assertions)]
                String::from("VK_LAYER_KHRONOS_validation"),
                String::from("VK_LAYER_MESA_overlay"),
            ]);
        }

        let instance = instance_builder.build()?;

        let mut surface = Surface::new(window, &instance)?;

        let logical_device = Rc::new(Device::new(&instance, &surface, true)?);

        let descriptor_manager = Rc::new(RefCell::new(
            DescriptorManager::new(logical_device.clone())?));

        surface.initialise_swapchain(window, &instance, logical_device.clone())?;

        let viewport_state = ViewportState::new(
            surface.get_current_swapchain_extent().width,
            surface.get_current_swapchain_extent().height);

        let depth_image = Image::new(
            logical_device.clone(),
            ash::vk::Extent3D::builder()
                .width(surface.get_current_swapchain_extent().width)
                .height(surface.get_current_swapchain_extent().height)
                .depth(1)
                .build(),
            ash::vk::Format::D32_SFLOAT_S8_UINT,
            ash::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
            ash::vk::ImageAspectFlags::DEPTH,
            1
        )?;

        let mut frames = Vec::new();
        (0..MAX_FRAMES_IN_FLIGHT)
            .for_each(|_| frames.push(FrameResources::new(logical_device.clone()).unwrap()));

        // Initialise all the empty slotmaps.
        let assets = Assets {
            meshes: SlotMap::with_key(),
            image_samplers: HashMap::new(),
            textures: SlotMap::with_key(),
            texture_path_lookup: HashMap::new(),
            descriptor_image_infos: SecondaryMap::new(),
            materials: SlotMap::with_key(),
        };

        log::info!("Finished renderer initialisation.");

        Ok(Self {
            m_instance: instance,
            m_surface: surface,
            m_logical_device: logical_device,
            m_viewport_state: viewport_state,
            m_descriptor_manager: descriptor_manager,
            m_depth_image: depth_image,
            m_current_frame: 0,
            m_frames: frames,

            m_resized_this_frame: false,

            m_camera: camera,

            m_assets: assets,

            m_shaders: HashMap::new(),
            m_renderables: Vec::new(),
        })
    }

    pub fn init_gui_state(&self) -> Result<GUIState, Box<dyn Error>> {
        let mut imgui_context = imgui::Context::create();
        imgui_context.set_ini_filename(None);
        let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui_context);
        let hidpi_factor = platform.hidpi_factor();
        let font_size = (13.0 * hidpi_factor) as f32;
        imgui_context.fonts().add_font(&[
            imgui::FontSource::DefaultFontData {
                config: Some(imgui::FontConfig {
                    size_pixels: font_size,
                    ..imgui::FontConfig::default()
                }),
            },
        ]);

        let imgui_renderer = imgui_vk::Renderer::with_gpu_allocator(
            self.m_logical_device.get_memory_allocator(),
            self.m_logical_device.get_device_handle().clone(),
            *self.m_logical_device.get_graphics_queue(),
            *self.m_logical_device.get_command_pool(),
            ash::vk::RenderPass::null(),
            &mut imgui_context,
            Some(imgui_vk::Options {
                in_flight_frames: 2,
                enable_depth_test: true,
                enable_depth_write: true,
                enable_dynamic_rendering: true,
                colour_attachment_format: self.m_surface.get_current_swapchain().get_image_format().format,
                depth_attachment_format: ash::vk::Format::D32_SFLOAT_S8_UINT,
                stencil_attachment_format: ash::vk::Format::D32_SFLOAT_S8_UINT,
            }),
        )?;

        Ok(GUIState {
            context: imgui_context,
            font_size: font_size,
            platform: platform,
            renderer: imgui_renderer,
        })
    }

    pub fn load_shaders(&mut self) -> Result<(), Box<dyn Error>> {
        // Iterate over every file in data/assets/shaders and create a Shader from any file with a
        // .spv file extension.
        for file in std::fs::read_dir("data/assets/shaders")? {
            let file = file?;
            let path = file.path();

            if path.extension().unwrap().to_str().unwrap() == "spv" {
                let shader = Shader::load(
                    &path,
                    self.m_logical_device.clone(),
                    self.m_descriptor_manager.clone())?;

                self.m_shaders.insert(
                    path.file_stem().unwrap().to_string_lossy().to_string(),
                    Rc::new(shader));
            }
        }

        Ok(())
    }


    pub fn load_scene(&mut self) -> Result<(), Box<dyn Error>> {
        let test_model = self
            .load_model("data/assets/meshes/botw/source/zeldaPosed001.fbx", 0.01)?;
        let mesh_handle = self.m_assets.meshes.insert(test_model);

        let mesh_pipeline_builder = Pipeline::builder()
            .colour_formats(vec![self.m_surface.get_current_swapchain().get_image_format().format])
            .cull_mode(ash::vk::CullModeFlags::NONE)
            .depth_clamp_enable(false)
            .depth_format(ash::vk::Format::D32_SFLOAT_S8_UINT)
            .front_face(ash::vk::FrontFace::CLOCKWISE)
            .line_width(1.0)
            .polygon_mode(ash::vk::PolygonMode::FILL)
            .shaders(vec![
                self.m_shaders["textured_mesh_vert"].clone(),
                self.m_shaders["textured_mesh_frag"].clone()])
            .stencil_format(ash::vk::Format::D32_SFLOAT_S8_UINT)
            .topology(ash::vk::PrimitiveTopology::TRIANGLE_LIST);

        let textured_mesh = Material {
            m_pipeline: mesh_pipeline_builder.build(
                self.m_logical_device.clone(),
                &self.m_viewport_state)?,
        };
        let material_handle = self.m_assets.materials.insert(textured_mesh);

        self.m_renderables.push(Renderable {
            m_material: material_handle,
            m_mesh: mesh_handle,
            m_transform_matrix: glm::Mat4::identity().append_translation(&glm::vec3(0.0, -1.0, -2.0))
        });

        Ok(())
    }

    pub fn draw_frame(&mut self, window: &Window, gui_state: &mut GUIState) {
        unsafe {
            gui_state.context.io_mut().display_size = [
                self.m_surface.get_current_swapchain_extent().width as f32,
                self.m_surface.get_current_swapchain_extent().height as f32];

            gui_state.platform.prepare_frame(
                gui_state.context.io_mut(),
                window).unwrap();

            let mut ui = gui_state.context.frame();
            let mut run = true;

            {
                let w = imgui::Window::new("Testing")
                    .opened(&mut run)
                    .position([20.0, 200.0], imgui::Condition::Appearing)
                    .size([200.0, 200.0], imgui::Condition::Appearing);
                w.build(&ui, || {
                    ui.text("Please work");
                });

                gui_state.platform.prepare_render(&ui, window);
            }

            let draw_data = ui.render();

            self.get_current_frame().await_render_finished_fence().unwrap();
            self.get_current_frame().reset_render_finished_fence().unwrap();

            // Could just reset the command pool here if there's multiple command buffers per frame.
            self.get_current_frame().reset_command_buffer().unwrap();

            let (image_index, result) = self
                .m_surface
                .get_current_swapchain()
                .get_swapchain_loader()
                .acquire_next_image(
                    *self
                        .m_surface
                        .get_current_swapchain()
                        .get_swapchain_handle(),
                    std::u64::MAX,
                    *self.get_current_frame().get_image_acquired_semaphore(),
                    ash::vk::Fence::null(),
                )
                .unwrap();

            self.get_current_frame()
                .begin_command_buffer(ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)
                .unwrap();

            self.m_logical_device.perform_image_layout_transition(
                self.get_current_frame().get_command_buffer(),
                self.m_surface.get_current_swapchain().get_images()[image_index as usize],
                ash::vk::ImageSubresourceRange::builder()
                    .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
                ash::vk::AccessFlags::empty(),
                ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                ash::vk::ImageLayout::UNDEFINED,
                ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                ash::vk::PipelineStageFlags::TOP_OF_PIPE,
                ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
            ).unwrap();

            self.m_logical_device.perform_image_layout_transition(
                self.get_current_frame().get_command_buffer(),
                *self.m_depth_image.get_image_handle(),
                ash::vk::ImageSubresourceRange::builder()
                    .aspect_mask(ash::vk::ImageAspectFlags::DEPTH | ash::vk::ImageAspectFlags::STENCIL)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
                ash::vk::AccessFlags::empty(),
                ash::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
                ash::vk::ImageLayout::UNDEFINED,
                ash::vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL,
                ash::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS
                    | ash::vk::PipelineStageFlags::LATE_FRAGMENT_TESTS,
                ash::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS
                    | ash::vk::PipelineStageFlags::LATE_FRAGMENT_TESTS
            ).unwrap();

            let colour_attachment_rendering_info = ash::vk::RenderingAttachmentInfoKHR::builder()
                .image_view(
                    self.m_surface
                        .get_current_swapchain()
                        .get_image_views()[image_index as usize],
                )
                .image_layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                .resolve_mode(ash::vk::ResolveModeFlags::NONE)
                .load_op(ash::vk::AttachmentLoadOp::CLEAR)
                .store_op(ash::vk::AttachmentStoreOp::STORE)
                .clear_value(ash::vk::ClearValue {
                    color: ash::vk::ClearColorValue {
                        float32: [0.1, 0.1, 0.1, 1.0],
                    },
                })
                .build();

            let depth_attachment_rendering_info = ash::vk::RenderingAttachmentInfoKHR::builder()
                .image_view(*self.m_depth_image.get_view())
                .image_layout(ash::vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL)
                .resolve_mode(ash::vk::ResolveModeFlags::NONE)
                .load_op(ash::vk::AttachmentLoadOp::CLEAR)
                .store_op(ash::vk::AttachmentStoreOp::DONT_CARE)
                .clear_value(ash::vk::ClearValue {
                    depth_stencil: ash::vk::ClearDepthStencilValue {
                        depth: 1.0,
                        stencil: 0,
                    },
                })
                .build();

            let render_area = ash::vk::Rect2D::builder()
                .extent(self.m_surface.get_current_swapchain_extent())
                .offset(*ash::vk::Offset2D::builder().x(0).y(0))
                .build();

            let rendering_info = ash::vk::RenderingInfoKHR::builder()
                .flags(ash::vk::RenderingFlagsKHR::empty())
                .render_area(render_area)
                .layer_count(0)
                .view_mask(0)
                .color_attachments(&[colour_attachment_rendering_info])
                .depth_attachment(&depth_attachment_rendering_info)
                .stencil_attachment(&depth_attachment_rendering_info)
                .build();

            let dynamic_rendering = ash::extensions::khr::DynamicRendering::new(
                self.m_instance.get_instance_handle(),
                self.m_logical_device.get_device_handle(),
            );

            dynamic_rendering.cmd_begin_rendering(*self.get_current_frame().get_command_buffer(), &rendering_info);

            self.m_logical_device.get_device_handle().cmd_set_viewport(
                *self.get_current_frame().get_command_buffer(),
                0,
                &[*self.m_viewport_state.get_viewport()],
            );
            self.m_logical_device.get_device_handle().cmd_set_scissor(
                *self.get_current_frame().get_command_buffer(),
                0,
                &[*self.m_viewport_state.get_scissor_rectangle()],
            );

            let aspect = self.m_surface.get_current_swapchain_extent().width as f32
                / self.m_surface.get_current_swapchain_extent().height as f32;

            let projection_matrix = glm::perspective_zo(aspect, 1.222, 0.1, 100.0);

            let view_matrix = glm::look_at(
                &self.m_camera.borrow().get_position(),
                &(self.m_camera.borrow().get_position() + self.m_camera.borrow().get_front()),
                &glm::vec3(0.0, 1.0, 0.0),
            );

            // Sort the renderables by material pointer to minimise pipeline binds.
            self.m_renderables.sort_unstable_by(|a, b| {
                a.m_material.partial_cmp(&b.m_material).unwrap()
            });

            let mut last_material = None;
            let mut last_mesh = None;
            for renderable in self.m_renderables.iter() {
                // If the materials are different, bind the new material's pipeline and, if necessary,
                // texture descriptor set.
                if last_material.is_none() || last_material.unwrap() != &renderable.m_material {
                    let material = self.m_assets.materials.get(renderable.m_material);

                    if material.is_none() {
                        log::warn!("Renderable is holding an invalid material handle.");
                    } else {
                        let material = material.unwrap();

                        self.m_logical_device.get_device_handle().cmd_bind_pipeline(
                            *self.get_current_frame().get_command_buffer(),
                            ash::vk::PipelineBindPoint::GRAPHICS,
                            *material.m_pipeline.get_pipeline_handle());

                        last_material = Some(&renderable.m_material);
                    }
                }

                if last_mesh.is_none() || last_mesh.unwrap() != &renderable.m_mesh {
                    let mesh = self.m_assets.meshes.get(renderable.m_mesh);

                    if mesh.is_none() {
                        log::warn!("Renderable is holding an invalid mesh handle.");
                    } else {
                        let mesh = mesh.unwrap();

                        self.m_logical_device.get_device_handle().cmd_bind_vertex_buffers(
                            *self.get_current_frame().get_command_buffer(),
                            0,
                            &[*mesh.m_vertex_buffer.get_buffer_handle()],
                            &[0]);

                        self.m_logical_device.get_device_handle().cmd_bind_index_buffer(
                            *self.get_current_frame().get_command_buffer(),
                            *mesh.m_index_buffer.get_buffer_handle(),
                            0,
                            ash::vk::IndexType::UINT32);

                        last_mesh = Some(&renderable.m_mesh);
                    }
                }

                renderable.draw(
                    self.m_logical_device.clone(),
                    &*self.get_current_frame().get_command_buffer(),
                    projection_matrix,
                    view_matrix,
                    &self.m_assets);
            }

            gui_state.renderer.cmd_draw(
                *self.get_current_frame().get_command_buffer(),
                draw_data).unwrap();

            dynamic_rendering.cmd_end_rendering(*self.get_current_frame().get_command_buffer());

            self.m_logical_device.perform_image_layout_transition(
                self.get_current_frame().get_command_buffer(),
                self.m_surface.get_current_swapchain().get_images()[image_index as usize],
                ash::vk::ImageSubresourceRange::builder()
                    .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
                ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                ash::vk::AccessFlags::empty(),
                ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                ash::vk::ImageLayout::PRESENT_SRC_KHR,
                ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE
            ).unwrap();

            self.get_current_frame().end_command_buffer().unwrap();

            let submit_info = ash::vk::SubmitInfo::builder()
                .wait_semaphores(&[*self.get_current_frame().get_image_acquired_semaphore()])
                .wait_dst_stage_mask(&[ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                .command_buffers(&[*self.get_current_frame().get_command_buffer()])
                .signal_semaphores(&[*self.get_current_frame().get_render_finished_semaphore()])
                .build();

            self.m_logical_device
                .get_device_handle()
                .queue_submit(
                    *self.m_logical_device.get_graphics_queue(),
                    &[submit_info],
                    *self.get_current_frame().get_render_finished_fence(),
                )
                .unwrap();

            let swapchains = [*self
                .m_surface
                .get_current_swapchain()
                .get_swapchain_handle()];

            let image_indices = [image_index];
            let present_info = ash::vk::PresentInfoKHR::builder()
                .wait_semaphores(&[*self.get_current_frame().get_render_finished_semaphore()])
                .swapchains(&swapchains)
                .image_indices(&image_indices)
                .build();

            let suboptimal = self
                .m_surface
                .get_current_swapchain()
                .get_swapchain_loader()
                .queue_present(*self.m_logical_device.get_graphics_queue(), &present_info)
                .unwrap();

            if suboptimal || self.m_resized_this_frame {
                let (width, height) = self.m_surface
                    .recreate_swapchain(
                        window,
                        &self.m_instance,
                        self.m_logical_device.clone(),
                    )
                    .unwrap();

                // Resize the depth image after the swapchain has been recreated.
                self.m_depth_image = Image::new(
                    self.m_logical_device.clone(),
                    ash::vk::Extent3D::builder()
                        .width(width)
                        .height(height)
                        .depth(1)
                        .build(),
                    ash::vk::Format::D32_SFLOAT_S8_UINT,
                    ash::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
                    ash::vk::ImageAspectFlags::DEPTH,
                    1
                )
                .unwrap();

                // Resize the viewport and scissor.
                self.m_viewport_state.update(width, height);

                self.m_resized_this_frame = false;
            }

            self.m_current_frame += 1;
        }
    }

    pub fn stop(&self) {
        unsafe {
            self.m_logical_device
                .get_device_handle()
                .device_wait_idle()
                .unwrap();
        }
    }

    pub fn notify_window_resized(&mut self) {
        self.m_resized_this_frame = true;
    }

    fn load_model(&mut self, path: &str, scale_factor: f32) -> Result<Mesh, Box<dyn Error>> {
        let mut scene = russimp::scene::Scene::from_file(
            path,
            vec![
                russimp::scene::PostProcess::CalculateTangentSpace,
                russimp::scene::PostProcess::Triangulate,
                russimp::scene::PostProcess::JoinIdenticalVertices,
                russimp::scene::PostProcess::SortByPrimitiveType,
                russimp::scene::PostProcess::GenerateNormals
            ],
        )?;

        let mut vertices: Vec<Vertex> = vec![];
        let mut vertex_offsets: Vec<usize> = vec![];
        let mut indices: Vec<u32> = vec![];
        let mut index_offsets: Vec<usize> = vec![];
        let mut textures: Vec<Option<TextureHandle>> = vec![];

        // A mapping of which texture handle corresponds to which material index.
        let mut texture_handles_mapping: Vec<TextureHandle> = vec![];
        for (material_index, material) in scene.materials.iter().enumerate() {
            for texture_entry in material.textures.iter() {
                for texture in texture_entry.1.iter() {
                    let model_path = Path::new(&path).parent().unwrap();
                    let texture_path = model_path
                        .join(texture.path.replace("\\", "/"))
                        .canonicalize().unwrap();

                    let texture_handle = add_texture(
                        texture_path.to_str().unwrap(),
                        &mut self.m_assets,
                        self.m_logical_device.clone(),
                        self.m_descriptor_manager.clone())?;

                    texture_handles_mapping.insert(material_index, texture_handle);
                }
            }
        }

        for mesh in scene.meshes.iter_mut() {
            vertex_offsets.push(vertices.len());

            for (vertex_index, vertex) in mesh.vertices.iter().enumerate() {
                vertices.push(Vertex {
                    m_position: glm::vec3(vertex.x * scale_factor, vertex.y * scale_factor, vertex.z * scale_factor),
                    m_colour: glm::vec3(1.0, 1.0, 1.0),
                    m_normal: glm::vec3(
                        mesh.normals[vertex_index].x,
                        mesh.normals[vertex_index].y,
                        mesh.normals[vertex_index].z),
                    m_texture_coords: match mesh.texture_coords[0].is_some() {
                        true => {
                            glm::vec2(
                                mesh.texture_coords[0].as_ref().unwrap()[vertex_index].x,
                                1.0 - mesh.texture_coords[0].as_ref().unwrap()[vertex_index].y)
                        }
                        false => {
                            glm::vec2(0.0, 0.0)
                        }
                    }
                });
            }

            index_offsets.push(indices.len());

            for face in mesh.faces.iter_mut() {
                indices.append(&mut face.0);
            }

            if mesh.material_index < texture_handles_mapping.len().try_into().unwrap() {
                textures.push(Some(texture_handles_mapping[mesh.material_index as usize]));
            }
        }

        let vertex_buffer = Buffer::new_populated(
            self.m_logical_device.clone(),
            ash::vk::BufferUsageFlags::VERTEX_BUFFER,
            vertices
        )?;

        let index_buffer = Buffer::new_populated(
            self.m_logical_device.clone(),
            ash::vk::BufferUsageFlags::INDEX_BUFFER,
            indices
        )?;

        Ok(Mesh {
            m_vertex_buffer: vertex_buffer,
            m_index_buffer: index_buffer,
            m_vertex_offsets: vertex_offsets,
            m_index_offsets: index_offsets,
            m_textures: textures,
        })
    }

    fn get_current_frame(&self) -> &FrameResources {
        &self.m_frames[self.m_current_frame % MAX_FRAMES_IN_FLIGHT]
    }
}

pub struct Mesh {
    m_vertex_buffer: Buffer<Vertex>,
    m_index_buffer: Buffer<u32>,

    m_vertex_offsets: Vec<usize>,
    m_index_offsets: Vec<usize>,

    m_textures: Vec<Option<TextureHandle>>,
}

pub struct Material {
    m_pipeline: Pipeline,
}

pub struct Renderable {
    m_material: MaterialHandle,
    m_mesh: MeshHandle,
    m_transform_matrix: glm::Mat4,
}

impl Renderable {
    pub fn draw(
        &self,
        device: Rc<Device>,
        command_buffer: &ash::vk::CommandBuffer,
        projection_matrix: glm::Mat4,
        view_matrix: glm::Mat4,
        assets: &Assets,
    ) {
        unsafe {
            // MVP matrix calculations and push constants.
            let push_constants = PushConstants {
                m_matrix: projection_matrix * view_matrix * self.m_transform_matrix,
            };

            let push_constants_size =
                std::mem::size_of::<PushConstants>() / std::mem::size_of::<u8>();
            let push_constants_pointer = &push_constants as *const PushConstants as *const u8;
            let push_constants_slice =
                std::slice::from_raw_parts(push_constants_pointer, push_constants_size);

            let material = &assets.materials[self.m_material];

            device.get_device_handle().cmd_push_constants(
                *command_buffer,
                *material.m_pipeline.get_layout(),
                ash::vk::ShaderStageFlags::VERTEX,
                0,
                push_constants_slice,
            );

            let mesh = &assets.meshes[self.m_mesh];

            for (index, vertex_offset) in mesh.m_vertex_offsets.iter().enumerate() {
                let texture_handle = mesh.m_textures.get(index).unwrap_or(&None);
                if texture_handle.is_some() {
                    //let texture = resource_manager.texture_from_handle(texture_handle.unwrap());
                    let texture = assets.textures.get(texture_handle.unwrap());
                    if texture.is_some() {
                        device.get_device_handle().cmd_bind_descriptor_sets(
                            *command_buffer,
                            ash::vk::PipelineBindPoint::GRAPHICS,
                            *material.m_pipeline.get_layout(),
                            0,
                            &[*texture.unwrap().get_descriptor_set()],
                            &[]);
                    }
                }

                // If there's another offset after this one then we can use that to calculate index count.
                // If not, use the length of the index buffer to calculate it instead.
                let indices_to_draw = if mesh.m_index_offsets.len() > index + 1 {
                    mesh.m_index_offsets[index + 1] - mesh.m_index_offsets[index]
                } else {
                    mesh.m_index_buffer.get_data().len() - mesh.m_index_offsets[index]
                };

                device.get_device_handle().cmd_draw_indexed(
                    *command_buffer,
                    indices_to_draw.try_into().unwrap(),
                    1,
                    mesh.m_index_offsets[index].try_into().unwrap(),
                    (*vertex_offset).try_into().unwrap(),
                    0,
                );
            }
        }
    }

    pub fn translate(&mut self, movement_vector: &glm::Vec3) {
        self.m_transform_matrix = glm::translate(&self.m_transform_matrix, movement_vector);
    }
}

pub struct GUIState {
    pub context: imgui::Context,
    pub font_size: f32,
    pub platform: imgui_winit_support::WinitPlatform,
    pub renderer: imgui_vk::Renderer,
}