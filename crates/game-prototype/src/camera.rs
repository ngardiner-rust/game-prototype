use std::ops::AddAssign;

use nalgebra_glm as glm;

pub struct Camera {
    m_position: glm::Vec3,
    m_front: glm::Vec3,
    m_yaw: f32,
    m_pitch: f32,
}

impl Camera {
    pub fn new() -> Self {
        Self {
            m_position: glm::vec3(0.0, 0.0, 0.0),
            m_front: glm::vec3(0.0, 0.0, -1.0),
            m_yaw: -90.0,
            m_pitch: 0.0,
        }
    }

    pub fn get_front(&self) -> glm::Vec3 {
        self.m_front
    }

    pub fn set_position(&mut self, x: f32, y: f32, z: f32) {
        self.m_position = glm::vec3(x, y, z);
    }

    pub fn get_position(&self) -> glm::Vec3 {
        self.m_position
    }

    pub fn set_rotation(&mut self, yaw: f32, pitch: f32) {
        self.m_yaw = yaw;
        self.m_pitch = pitch.clamp(-89.99, 89.99);

        self.m_front = glm::normalize(&glm::vec3(
            self.m_yaw.to_radians().cos() * self.m_pitch.to_radians().cos(),
            self.m_pitch.to_radians().sin(),
            self.m_yaw.to_radians().sin() * self.m_pitch.to_radians().cos(),
        ));
    }

    pub fn translate(&mut self, x: f32, y: f32, z: f32) {
        self.m_position.add_assign(z * self.m_front);
        self.m_position
            .add_assign(glm::normalize(&glm::cross(&self.m_front, &glm::vec3(0.0, 1.0, 0.0))) * x);
    }

    pub fn rotate(&mut self, yaw: f32, pitch: f32) {
        self.set_rotation(self.m_yaw + yaw, self.m_pitch + pitch);
    }
}
