use std::{collections::HashMap, rc::Rc, error::Error, cell::RefCell};

use slotmap::{ new_key_type, SecondaryMap, SlotMap };

use crate::rendering::*;
use rendering_engine::*;

new_key_type! { pub struct MeshHandle; }
new_key_type! { pub struct TextureHandle; }
new_key_type! { pub struct MaterialHandle; }

pub struct Assets {
    pub meshes: SlotMap<MeshHandle, Mesh>,

    pub image_samplers: HashMap<TextureHandle, ash::vk::Sampler>,

    pub textures: SlotMap<TextureHandle, Texture>,
    // Maps the path to the handle for quick lookup when loading models.
    pub texture_path_lookup: HashMap<String, TextureHandle>,
    pub descriptor_image_infos: SecondaryMap<TextureHandle, ash::vk::DescriptorImageInfo>,

    pub materials: SlotMap<MaterialHandle, Material>,
}

/// If the texture already exists in the map, return the handle for it. If not, add it and return
/// the newly created handle.
pub fn add_texture(
    texture_path: &str,
    assets: &mut Assets,
    device: Rc<Device>,
    descriptor_manager: Rc<RefCell<DescriptorManager>>
) -> Result<TextureHandle, Box<dyn Error>> {
    Ok(assets.texture_path_lookup.get(texture_path).cloned().unwrap_or_else(|| {
        assets.textures.insert(
            Texture::new(device, texture_path, descriptor_manager).unwrap())
    }))
}