mod assets;
use assets::*;

mod camera;
use camera::*;

mod rendering;

use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent, DeviceEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use std::{cell::RefCell, rc::Rc};

fn main() {
    // Initialise env_logger to show every of the level 'trace' and above.
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("trace")).init();
    log::info!("Initialised logging.");

    let event_loop = EventLoop::new();

    let window = WindowBuilder::new()
        .with_title("Game Prototype")
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)
        .unwrap();
    window.set_cursor_grab(true).unwrap();
    window.set_cursor_visible(false);

    let camera = Rc::new(RefCell::new(Camera::new()));
    let mut renderer = rendering::Renderer::new(&window, camera.clone()).unwrap();
    renderer.load_shaders().unwrap();
    renderer.load_scene().unwrap();

    let mut gui_state = renderer.init_gui_state().unwrap();

    log::info!("Entered main game loop.");

    let mut should_draw = true;

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        gui_state.platform.handle_event(gui_state.context.io_mut(), &window, &event);

        match event {
            Event::MainEventsCleared => {
                if should_draw {
                    renderer.draw_frame(&window, &mut gui_state);
                }
            }
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => {
                    log::info!("Window close requested. Exiting.");
                    *control_flow = ControlFlow::Exit;
                    should_draw = false;
                    renderer.stop();
                }
                WindowEvent::Resized { .. } => {
                    renderer.notify_window_resized();
                }
                WindowEvent::KeyboardInput { input, .. } => {
                    if let winit::event::KeyboardInput {
                        state: winit::event::ElementState::Pressed,
                        virtual_keycode: Some(keycode),
                        ..
                    } = input
                    {
                        match keycode {
                            winit::event::VirtualKeyCode::W => {
                                camera.borrow_mut().translate(0.0, 0.0, 0.1);
                            }
                            winit::event::VirtualKeyCode::A => {
                                camera.borrow_mut().translate(-0.1, 0.0, 0.0);
                            }
                            winit::event::VirtualKeyCode::S => {
                                camera.borrow_mut().translate(0.0, 0.0, -0.1);
                            }
                            winit::event::VirtualKeyCode::D => {
                                camera.borrow_mut().translate(0.1, 0.0, 0.0);
                            }
                            _ => {}
                        }
                    }
                }
                _ => (),
            },
            Event::DeviceEvent { event, .. } => match event {
                DeviceEvent::MouseMotion { delta, .. } => {
                    camera
                        .borrow_mut()
                        .rotate((delta.0 / 8.0) as f32, (-delta.1 / 8.0) as f32);
                }
                _=> (),
            }
            _ => (),
        }
    });
}
